#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>

typedef unsigned char (*fptr)();

static unsigned char dummy = 0;
static char *dummy2 = "";
extern void set_tilespalette();
extern unsigned char currentmusic;
extern unsigned char bgmusic;
extern void HandyMusic_StopMusic ();
extern void __fastcall__ HandyMusic_LoadPlayBGM(unsigned char filenr);
extern int GAME_FILENR;
extern unsigned char game(void);

static void fixmusic(unsigned char nextmusic)
{
    if (currentmusic == nextmusic) return;
    currentmusic = nextmusic;
    if (bgmusic) {
        HandyMusic_StopMusic ();
        HandyMusic_LoadPlayBGM(currentmusic);
    }
}

fptr loader (unsigned char index)
{
    switch(index) {
    default:
        lynx_load((int)&GAME_FILENR);
        fixmusic(0);
        return game;
    }
}
