MusicDrums 100
{
	pan $ff
	loop -1
		call DrumsA		; 256
	endloop
	end
}

DrumsA
{
	loop 2
		loop 3
			using Kick
			sfx 16
			sfx 16

			using Cymbal
			sfx 24
			using Tish
			sfx 4
			sfx 4

			rest 8
			using Kick
			sfx 8
	        	using SnareShort
			sfx 16

			using Cymbal
			sfx 8
			using Kick
			sfx 24
		endloop
		using Kick
		sfx 16
		sfx 16
		using Cymbal
		sfx 24
		using Tish
		sfx 4
		sfx 4
		rest 8
		using Kick
		sfx 8
	        using SnareShort
		sfx 16
		using Cymbal
		sfx 8
		using Kick
		sfx 8
	        using SnareShort
		sfx 8
		rest 8
	endloop
	return
}

