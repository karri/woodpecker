#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>

extern unsigned char checkInput();
extern unsigned char reset;

extern void __fastcall__ HandyMusic_PlaySFX(unsigned char sfx);
extern void HandyMusic_StopMusic();
extern void HandyMusic_StopSoundEffect();

static char* dummy = "ABC";

static int progress;
static int created;

extern unsigned char tree[];
extern unsigned char plane1[];
extern unsigned char plane2[];
extern unsigned char broken[];
extern unsigned char shoote[];
extern unsigned char shootne[];
extern unsigned char shootse[];
extern unsigned char pencil[];
extern unsigned char pencilhit[];
extern unsigned char beak[];
extern unsigned char beakbkg[];

typedef struct {
    unsigned char sprctl0;
    unsigned char sprctl1;
    unsigned char sprcoll;
    char *next;
    unsigned char *data;
    signed int hpos;
    signed int vpos;
    unsigned int hsize;
    unsigned int vsize;
    unsigned char h;
} sprite_t;

static sprite_t Spr57 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    0,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr56 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr57,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr55 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr56,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr54 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr55,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr53 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr54,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr52 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr53,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr51 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr52,
    pencil,
    100, 40,
    0x100, 0x100,
    100
};

static sprite_t Spr50 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr51,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr49 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr50,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr48 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr49,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr47 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr48,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr46 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr47,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr45 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr46,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr44 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr45,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr43 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr44,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr42 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr43,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr41 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr42,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr40 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr41,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr39 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr40,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr38 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr39,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr37 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr38,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr36 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr37,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr35 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr36,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr34 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr35,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr33 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr34,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr32 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr33,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr31 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr32,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr30 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr31,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr29 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr30,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr28 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr29,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr27 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr28,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr26 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr27,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr25 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr26,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr24 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr25,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr23 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr24,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr22 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr23,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr21 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr22,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr20 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr21,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr19 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr20,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr18 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr19,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr17 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr18,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr16 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr17,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr15 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr16,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr14 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr15,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr13 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr14,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr12 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr13,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr11 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr12,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr10 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr11,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr09 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr10,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr08 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr09,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr07 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr08,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr06 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr07,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr05 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr06,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr04 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr05,
    shoote,
    35, 20,
    0x100, 0x100,
    1
};

static sprite_t Spr03 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr04,
    shoote,
    35, 30,
    0x100, 0x100,
    1
};

static sprite_t Spr02 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr03,
    shoote,
    35, 40,
    0x100, 0x100,
    1
};

static sprite_t Spr01 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV | REUSEPAL | SKIP,
    NO_COLLIDE,
    (char *)&Spr02,
    shoote,
    35, 50,
    0x100, 0x100,
    1
};

static SCB_REHV_PAL Spr00 = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr01,
    plane1,
    35, 50,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Stree = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr00,
    tree,
    0, 0,
    160 * 0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static unsigned char endgame() {
    if (reset) return 0;
    //if (startcredits) return 0;
    return 1;
}

static clock_t shottime;

static unsigned char hit(sprite_t *Spr) {
    sprite_t *Tgt = (sprite_t*)&Spr50;
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) == 0) {
            if ((Spr->hpos > Tgt->hpos) && (Spr->hpos < Tgt->hpos + 43)) {
                if (Spr->vpos > Tgt->vpos) {
                    // Down
                    if (Spr->vpos < Tgt->vpos + 6) {
                        Spr->data = shootse;
                        Tgt->data = pencilhit;
                        if (Tgt->h) {
                            Tgt->h = Tgt->h - 1;
                        }
                        if (Tgt->h == 0) {
                            Tgt->sprctl1 = PACKED | REHV | REUSEPAL | SKIP;
                        }
                        return 1;
                    }
                } else {
                    // Up
                    if (Spr->vpos > Tgt->vpos - 8) {
                        Spr->data = shootne;
                        Tgt->data = pencilhit;
                        if (Tgt->h) {
                            Tgt->h = Tgt->h - 1;
                        }
                        if (Tgt->h == 0) {
                            Tgt->sprctl1 = PACKED | REHV | REUSEPAL | SKIP;
                        }
                        return 1;
                    }
                }
            }
        }
    }
    return 0;
}

static void planecollision() {
    sprite_t *Tgt = (sprite_t*)&Spr50;
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) == 0) {
            if ((Spr00.hpos > Tgt->hpos) && (Spr00.hpos < Tgt->hpos + 38)) {
                Spr00.data = broken;
            }
        }
    }
}

static void cleareffects() {
    sprite_t *Tgt = (sprite_t*)&Spr50;
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) == 0) {
            if (Tgt->data == pencilhit) {
                Tgt->data = pencil;
            }
        }
    }
}

static clock_t peck;
static unsigned char peckcount = 3;

static void movetgts() {
    sprite_t *Tgt = (sprite_t*)&Spr50;
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) == 0) {
            if (Tgt->data == beak) {
                if (peckcount > 0) {
                    if (Tgt->data == beak) {
                        switch (Tgt->vpos) {
                        case -100:
                            Tgt->vpos = 0;
                            break;
                        case 0:
                            Tgt->vpos = -40;
                            break;
                        default:
                            Tgt->vpos = -100;
                            if (peckcount) peckcount -= 1;
                            break;
                        }
                    }
                }
            }
            if (Tgt->data == pencil) {
                Tgt->hpos -= 1;
            }
            if (Tgt->data == pencilhit) {
                Tgt->hpos -= 1;
            }
        }
    }
}

static void posshots() {
    if (clock() > shottime) {
        sprite_t *Spr = (sprite_t*)&Spr00;
        cleareffects();
        movetgts();
        while (Spr->next != (char *)&Spr51) {
            Spr = (sprite_t*)(Spr->next);
            if ((Spr->sprctl1 & SKIP) == 0) {
                if (Spr->hpos < 159 + 4) {
                    hit(Spr);
                    if (Spr->data == shoote) {
                        Spr->hpos += 4;
                    }
                    if (Spr->data == shootne) {
                        Spr->hpos += 2;
                        Spr->vpos -= 2;
                    }
                    if (Spr->data == shootse) {
                        Spr->hpos += 2;
                        Spr->vpos += 2;
                    }
                } else {
                    Spr->sprctl1 = PACKED | REHV | REUSEPAL | SKIP;
                }
            }
        }
        if ((Spr00.data == broken) && (Spr00.vpos < 95)) {
            Spr00.vpos += 1;
        }
        shottime = clock() + 2;
    }
}

static void newshot() {
    sprite_t *Spr = (sprite_t*)&Spr00;
    while (Spr->next != 0) {
        Spr = (sprite_t*)(Spr->next);
        if ((Spr->sprctl1 & SKIP) != 0) {
            Spr->sprctl1 = PACKED | REHV | REUSEPAL;
            Spr->hpos = Spr00.hpos;
            Spr->vpos = Spr00.vpos;
            Spr->data = shoote;
            return;
        }
    }
}

static void newtgt(int x, int y, char *bmp) {
    sprite_t *Tgt = (sprite_t*)&Spr50;
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) != 0) {
            Tgt->sprctl1 = PACKED | REHV | REUSEPAL;
            Tgt->hpos = x;
            Tgt->vpos = y;
            Tgt->data = bmp;
            Tgt->h = 20;
            return;
        }
    }
}

static void createtgts() {
    switch (created >> 7) {
    default:
    case 0:
        newtgt(100 + 70, 50, pencil);
        break;
    case 6:
        newtgt(100 + 70, 20, pencil);
        newtgt(100 + 90, 35, pencil);
        break;
    case 2:
        newtgt(100 + 70, 20, pencil);
        newtgt(100 + 90, 35, pencil);
        newtgt(100 + 70, 80, pencil);
        break;
    case 3:
        newtgt(100 + 70, 20, pencil);
        newtgt(100 + 90, 35, pencil);
        newtgt(100 + 90, 65, pencil);
        newtgt(100 + 70, 80, pencil);
        break;
    case 5:
    case 20:
        newtgt(100 + 70, 20, pencil);
        newtgt(100 + 90, 35, pencil);
        newtgt(100 + 70, 50, pencil);
        newtgt(100 + 90, 65, pencil);
        newtgt(100 + 70, 80, pencil);
        break;
    case 1:
    case 12:
    case 18:
        newtgt(100 + 70, 0, beakbkg);
        newtgt(100 + 70, -100, beak);
        break;
    }
}

static void movescenery(int x) {
    sprite_t *Tgt = (sprite_t*)&Spr50;
    progress -= x;
    if ((progress & 0x7f80) > (created & 0x7f80)) {
        createtgts();
        created = progress;
    }
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) == 0) {
            Tgt->hpos += x;
        }
    }
}

static unsigned char nrtgt() {
    unsigned char count = 0;
    sprite_t *Tgt = (sprite_t*)&Spr50;
    while (Tgt->next != 0) {
        Tgt = (sprite_t*)(Tgt->next);
        if ((Tgt->sprctl1 & SKIP) == 0) {
            count = count + 1;
        }
    }
    return count;
}

// Woodpecker game
unsigned char game() 
{
    unsigned char ret = 0;
    unsigned char joy;
    clock_t up = clock();
    clock_t down = clock();
    clock_t left = clock();
    clock_t right = clock();
    clock_t prop = clock();
    clock_t rush = clock();
    clock_t rushtime = clock();
    clock_t shoot = clock();
    peck = clock();
    shottime = clock();
    tgi_setpalette(tgi_getdefpalette());
    progress = 0;
    created = progress;
    createtgts();
    while (endgame()) {
        joy = checkInput();
        if (JOY_BTN_UP(joy) && clock() > up && Spr00.data != broken) {
            up = clock() + 1;
            Spr00.vpos = Spr00.vpos - 1;
        }
        if (JOY_BTN_DOWN(joy) && clock() > down && Spr00.data != broken) {
            down = clock() + 1;
            Spr00.vpos = Spr00.vpos + 1;
        }
        if (JOY_BTN_RIGHT(joy) && clock() > right && Spr00.data != broken) {
            right = clock() + 1;
            if (Spr00.hpos < 80) {
                Spr00.hpos = Spr00.hpos + 1;
            } else {
                movescenery(-1);
            }
        }
        if (JOY_BTN_LEFT(joy) && clock() > left && Spr00.data != broken) {
            left = clock() + 1;
            if (Spr00.hpos > 30) {
                Spr00.hpos = Spr00.hpos - 1;
            } else {
                movescenery(1);
            }
        }
        if (JOY_BTN_FIRE2(joy) && clock() > rush && Spr00.data != broken) {
            Spr00.hsize = 0x200;
            Spr00.hpos += 80;
            if (Spr00.hpos > 159) {
                Spr00.hpos = 159;
            }
            if (Spr00.hpos > 79) {
                movescenery(-(Spr00.hpos - 79));
                Spr00.hpos = 79;
            }
            rushtime = clock() + 5;
            rush = clock() + 5 * 60;
        }
        if (Spr00.hsize == 0x200 && clock() > rushtime) {
            Spr00.hsize = 0x100;
        }
        if (JOY_BTN_FIRE(joy) && clock() > shoot && Spr00.data != broken) {
            newshot();
            shoot = clock() + 5;
        }
        if (clock() > peck) {
            peckcount = 3;
            peck = clock() + 2 * 60;
        }
        posshots();
        planecollision();
        if (!tgi_busy()) {
            tgi_sprite(&Stree);
            tgi_updatedisplay();
            if (clock() > prop) {
                if (Spr00.data == plane1) {
                    Spr00.data = plane2;
                } else {
                    Spr00.data = plane1;
                }
                prop = prop + 10;
            }
        }
    }
    while (checkInput() != 0)
        ;
    return ret;
} 

