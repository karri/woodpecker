/*
 * The code and data defined here is placed in resident RAM
 */
#include <lynx.h>
#include <stdlib.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

typedef unsigned char (*fptr)();
extern fptr loader (unsigned char index);

extern unsigned char Sample_Playing;
#pragma zpsym("Sample_Playing");

signed char SDCheck = -1;	// tracks if there is a SD cart with saves enabled. -1 = No check yet, 0 = no SD saves, 1 = SD saves possible 
unsigned char check_eeprom = 1;
unsigned char startcredits = 0;
unsigned char bgmusic = 1;
unsigned char halted = 0;
unsigned char reset = 0;
unsigned char currentmusic = 4;

extern int KEYHANDLER_FILENR;
extern int LOADER_FILENR;
extern int INTRO_FILENR;
extern void initsystem();
extern void intro(void);
extern int BOOTANIM_FILENR;
extern void boot(void);
extern int HM_FILENR;
extern int SFX_FILENR;
extern int MUSIC_FILENR;

unsigned char level = 1;

static unsigned char loadandexec(unsigned char index)
{
    fptr lvl;

    lynx_load((int)&LOADER_FILENR);
    lvl = loader(index);
    lynx_load((int)&KEYHANDLER_FILENR);
    return lvl();
}

int main(void)
{
    clock_t starttime;
    unsigned char next_block;

    lynx_load((int)&INTRO_FILENR);
    initsystem();
    //lynx_load((int)&BOOTANIM_FILENR);
    //boot();
    next_block = 1;
    while (1) {
        unsigned char ret;
        switch (next_block) {
        default:
            ret = loadandexec(next_block);
            break;
        case 0:
            // Show intro at startup
            lynx_load((int)&INTRO_FILENR);
            intro();
            if (reset == 0) {
            }
            break;
        }
        if (reset) {
            reset = 0;
            next_block = 0;
        }
    }
    return 0;
}

