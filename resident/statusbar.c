// Statusbar stuff
#include <lynx.h>
#include <stdlib.h>
#include <string.h>
#include <tgi.h>

extern char heart0[];
extern char heart1[];
extern char heart2[];
extern char heart3[];
extern char heart4[];
extern char heart5[];
extern char heart6[];
extern char key[];
extern char bullet[];
extern char bomb[];
extern char miina1[];
extern char H0[];
extern char H1[];
extern char H2[];
extern char H3[];
extern char H4[];
extern char H5[];
extern char H6[];
extern char H7[];
extern char H8[];
extern char H9[];
extern char Hcolon[];

unsigned char health = 6;
unsigned char target_saved = 0;
unsigned char target_health = 4;
unsigned char ammo = 0;
unsigned char bombs = 0;
unsigned char mines = 0;
unsigned char keys = 0;
unsigned char devices = 0;
unsigned char docs = 0;
unsigned char arrival = 0;
unsigned char enemies_killed = 0;
unsigned char rebel = 1;
int top;
int topoff;
int left;
int leftoff;

typedef struct {
    const unsigned char *bitmap;
    unsigned char width;
} font_t;

static font_t numbers[] = {
    {H0, 7},
    {H1, 4},
    {H2, 5},
    {H3, 5},
    {H4, 6},
    {H5, 5},
    {H6, 6},
    {H7, 5},
    {H8, 6},
    {H9, 6},
    {Hcolon, 2}
};

static SCB_REHV_PAL ch3 = {
    BPP_3 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 0,
    0x100, 0x100,
    {0x03,0x59, 0xac, 0xee}
};

void tgi_numbers(int x, int y, char *msg, unsigned char colour) {
    unsigned char index;
    unsigned char w;
    ch3.hpos = x;
    ch3.vpos = y;
    switch (colour) {
    case 0:
        ch3.penpal[0] = 0x03;
        ch3.penpal[1] = 0x59;
        ch3.penpal[2] = 0xac;
        ch3.penpal[3] = 0xee;
        break;
    case 1: // Pacifist white
        ch3.penpal[0] = 0x03;
        ch3.penpal[1] = 0x33;
        ch3.penpal[2] = 0x33;
        ch3.penpal[3] = 0x33;
        break;
    case 2: // Killer red
        ch3.penpal[0] = 0x0f;
        ch3.penpal[1] = 0xff;
        ch3.penpal[2] = 0xff;
        ch3.penpal[3] = 0xff;
        break;
    }
    while (*msg) {
        w = 4;
        if (*msg >= '0' && *msg <= '9') {
            index = *msg - '0';
            w = numbers[index].width + 1;
            ch3.data = (unsigned char *)numbers[index].bitmap;
            tgi_sprite(&ch3);
        }
        if (*msg == ':') {
            w = numbers[10].width + 1;
            ch3.data = (unsigned char *)numbers[10].bitmap;
            tgi_sprite(&ch3);
        }
        ch3.hpos = ch3.hpos + w;
        msg = msg + 1;
    }
}

static unsigned char tgi_len(char *msg) {
    unsigned char index;
    unsigned char w;
    unsigned char msglen = 2;
    while (*msg) {
        w = 4;
        if (*msg >= '0' && *msg <= '9') {
            index = *msg - '0';
            w = numbers[index].width + 1;
        }
        msglen = msglen + w;
        msg = msg + 1;
    }
    return msglen;
}

static SCB_REHV_PAL Sstatusbar = {
    BPP_4 | TYPE_NORMAL,
    LITERAL | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 4,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static unsigned char
drawstatusbarobj(unsigned char val, char *bm, unsigned char endpos, unsigned char adj)
  {
    if (val) {
        char buf[4];

        endpos -= 14 - adj;
        Sstatusbar.hpos = endpos;
        Sstatusbar.data = bm;
        tgi_sprite(&Sstatusbar);

        itoa(val, buf, 10);
        tgi_setcolor(14);
        endpos -= tgi_len(buf);
        tgi_numbers(endpos,0,buf, 0);
    }
    return endpos;
  }

static unsigned char heart_flipping = 0;

static unsigned char draw_heart(unsigned char myhealth, unsigned char endpos)
{
    endpos -= 8;
    heart_flipping = (heart_flipping + 1) & 7;
    switch (myhealth) {
    case 6:
        //Sstatusbar.data = heart0;
        break;
    case 5:
        //Sstatusbar.data = heart1;
        break;
    case 4:
        //Sstatusbar.data = heart2;
        break;
    case 3:
        //Sstatusbar.data = heart3;
        break;
    case 2:
        //Sstatusbar.data = heart4;
        break;
    case 1:
        //if (heart_flipping > 3)
            //Sstatusbar.data = heart5;
        //else
            //Sstatusbar.data = heart6;
        break;
    case 0:
        return endpos;
    }
    Sstatusbar.hpos = endpos;
    tgi_sprite(&Sstatusbar);
    return endpos;
}

void draw_status_bar()
{
    unsigned char endpos = 159;

    tgi_sprite(&Sstatusbar);
    tgi_setcolor(1);
    tgi_bar(0,0,159,7);
    endpos = draw_heart(health, endpos);
    if (target_saved) {
        endpos = draw_heart(target_health, endpos);
    }
    //endpos = drawstatusbarobj(ammo, bullet, endpos, 4);
    //endpos = drawstatusbarobj(bombs, bomb, endpos, 6);
    //endpos = drawstatusbarobj(keys, key, endpos, 0);
    //endpos = drawstatusbarobj(mines, miina1, endpos, 0);
    endpos = 40;
}
