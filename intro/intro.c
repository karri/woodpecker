#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>
#include "LynxSD.h"

extern void HandyMusic_Init ();
extern void HandyMusic_PlayMusic ();
extern void HandyMusic_Pause ();
extern void HandyMusic_UnPause ();
extern void HandyMusic_Main ();
extern void HandyMusic_StopAll();
extern void HandyMusic_StopMusic ();
extern void __fastcall__ HandyMusic_PlaySFX(unsigned char sfx);
extern void HandyMusic_StopSoundEffect (unsigned char priority);
extern void __fastcall__ HandyMusic_LoadPlayBGM(unsigned char filenr);
extern unsigned __fastcall__ lnx_eeprom_read (unsigned char cell);
extern void __fastcall__ lnx_eeprom_write (unsigned char cell, unsigned val);
extern unsigned char bgmusic;
extern unsigned char currentmusic;
extern unsigned char halted;
extern unsigned char reset;

static const char SDSavePath[] = "/saves/onduty.sav";
extern signed char SDCheck;	// tracks if there is a SD cart with saves enabled. -1 = No check yet, 0 = no SD saves, 1 = SD saves possible 

extern unsigned char checkInput ();
extern unsigned char startcredits;

extern int KEYHANDLER_FILENR;
extern int HM_FILENR;
extern int SFX_FILENR;
extern int MUSIC_FILENR;
extern int TILE_FILENR;

static char black_pal[] = {
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,

    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
};

static char title_pal[] = {
    0x0fff >> 8,
    0x0000 >> 8,
    0x0220 >> 8,
    0x090e >> 8,
    0x0d5d >> 8,
    0x000f >> 8,
    0x0947 >> 8,
    0x0749 >> 8,
    0x0120 >> 8,
    0x0545 >> 8,
    0x0507 >> 8,
    0x0405 >> 8,
    0x0878 >> 8,
    0x08a8 >> 8,
    0x0bd9 >> 8,
    0x0685 >> 8,

    0x0fff & 0xff,
    0x0000 & 0xff,
    0x0220 & 0xff,
    0x090e & 0xff,
    0x0d5d & 0xff,
    0x000f & 0xff,
    0x0947 & 0xff,
    0x0749 & 0xff,
    0x0120 & 0xff,
    0x0545 & 0xff,
    0x0507 & 0xff,
    0x0405 & 0xff,
    0x0878 & 0xff,
    0x08a8 & 0xff,
    0x0bd9 & 0xff,
    0x0685 & 0xff,
};

extern unsigned char title[];
extern unsigned char On[];
extern unsigned char Duty[];
extern unsigned char Effect[];
extern unsigned char Lamp1[];
extern unsigned char Lamp2[];
extern unsigned char smoke1[];
extern unsigned char smoke2[];
extern unsigned char smoke3[];
extern unsigned char smoke4[];
extern unsigned char check_eeprom;
static clock_t completedmissions[32];
extern unsigned char rebel;

// Bit usage for the last long eeprom
// Bits 0..29 pacifist bits
// Bit 30 feelgood
// Bit 31 rebel
// This means that levels 0..29 have a pacifist bit
// Slot 30 is not used at all
static unsigned char get_bit_in_last_long(unsigned char bit)
{
    unsigned int *buf = (unsigned int *)&completedmissions[31];

    if (bit < 16) {
        return (buf[0] >> bit) & 1;
    } else {
        return (buf[1] >> (bit - 16)) & 1;
    }
}

static void set_bit_in_last_long(unsigned char bit, unsigned char val)
{
    unsigned int ival = val;
    unsigned int *buf = (unsigned int *)&completedmissions[31];

    if (bit < 16) {
        buf[0] |= (ival << bit);
    } else {
        buf[1] |= (ival << (bit - 16));
    }
}

static SCB_REHV_PAL SprText = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    On,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    title,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static void drawOn(int x, int y)
{
    SprText.hpos = x;
    SprText.vpos = y;
    SprText.sprctl0 = BPP_4 | TYPE_NORMAL;
    SprText.data = On;
    tgi_sprite(&SprText);
}

static void drawDuty(int x, int y)
{
    SprText.hpos = x;
    SprText.vpos = y;
    SprText.sprctl0 = BPP_4 | TYPE_NORMAL;
    SprText.data = Duty;
    tgi_sprite(&SprText);
}

static void drawEffect(int x, int y, int x2)
{
    SprText.hpos = x;
    SprText.vpos = y;
    SprText.sprctl0 = BPP_4 | TYPE_NORMAL;
    SprText.data = Effect;
    tgi_sprite(&SprText);
    SprText.hpos = x2;
    SprText.sprctl0 = BPP_4 | TYPE_NORMAL | HFLIP;
    SprText.data = Effect;
    tgi_sprite(&SprText);
}

static void drawLamps1()
{
    SprText.sprctl0 = BPP_4 | TYPE_BACKGROUND;
    SprText.data = Lamp1;
    SprText.vpos = 63;
    SprText.hpos = 17;
    tgi_sprite(&SprText);
    SprText.hpos = 129;
    tgi_sprite(&SprText);
}

static void drawLamps2()
{
    SprText.sprctl0 = BPP_4 | TYPE_BACKGROUND;
    SprText.data = Lamp2;
    SprText.vpos = 65;
    SprText.hpos = 2;
    tgi_sprite(&SprText);
    SprText.hpos = 145;
    tgi_sprite(&SprText);
}

static unsigned char smokeindex = 0;

static void drawSmoke()
{
    SprText.sprctl0 = BPP_4 | TYPE_NORMAL;
    switch (smokeindex >> 3) {
    case 0:
        SprText.data = smoke1;
        break;
    case 1:
        SprText.data = smoke2;
        break;
    case 2:
        SprText.data = smoke3;
        break;
    case 3:
        SprText.data = smoke4;
        break;
    }
    SprText.vpos = 63-14;
    SprText.hpos = 117 - 3;
    tgi_sprite(&SprText);
    smokeindex = (smokeindex + 1) & 31;
}

static signed char falling[] = {0, -1, 0, 1, 0, -1, 0, 1, 0, -2, 0, 2, 0, -3, 0, 3, 0, -4, 0, -4,
0, 5, 0, -5, 0, 6, 0, -6, 0, 6, 10, 20};

extern unsigned char HandyMusic_BGMPlaying;
#pragma zpsym("HandyMusic_BGMPlaying");
extern unsigned char HandyMusic_Active;
#pragma zpsym("HandyMusic_Active");

void HandyMusic_Init ();
void HandyMusic_PlayMusic ();
void HandyMusic_UnPause ();
void HandyMusic_Main ();

void initsystem()
{
    lynx_load((int)&KEYHANDLER_FILENR);
    lynx_load((int)&SFX_FILENR);
    lynx_load((int)&HM_FILENR);
    tgi_install(&tgi_static_stddrv); // This will activate the Lynx screen 
    joy_install(&joy_static_stddrv); // This will activate the Lynx joypad
    tgi_init();
    tgi_setpalette(black_pal);
    tgi_setframerate(60);
    HandyMusic_Init();
    CLI();
    while (tgi_busy()) ;
    tgi_init();
    while (tgi_busy()) ;
}

static unsigned char endgame() {
    if (reset) return 0;
    if (JOY_BTN_FIRE(checkInput())) return 0;
    if (startcredits) return 0;
    return 1;
}

static void init_eeprom() {
    memset((unsigned char *)completedmissions, 0, 128);
    //completedmissions[31] = 0x3ffffffe;
    // If feelgood achievement has been made - keep it
    // For complete erasal enter main storyline and clear eeprom
}

static void get_feelgood_and_rebel() {
}

static void set_feelgood_and_rebel() {
}

// The last 4 bytes of the eeprom are for pacifist bits
static unsigned char ispacifist(unsigned int mission) {
    unsigned int *buf = (unsigned int *)&completedmissions[31];
    if (mission < 16) {
        return (buf[0] & ((unsigned int)1 << mission)) != 0;
    }
    return (buf[1] & ((unsigned int)1 << (mission - 16))) != 0;
}

#define LAST_REBEL 29

static unsigned char isfeelgood()
{
    unsigned char l;
    for (l = 0; l < 10; l++) {
        if (ispacifist(l) == 0) return 0;
    }
    for (l = 20; l <= LAST_REBEL; l++) {
        if (ispacifist(l) == 0) return 0;
    }
    return 1;
}

void intro(void) 
{
    clock_t now = clock();
    clock_t blink;
    clock_t blink2;
    unsigned char OnDrop = 31;
    unsigned char OnDropDelay = 8;
    unsigned char DutyDrop = 31;
    unsigned char DutyDropDelay = 8;
    unsigned char EffectDrop = 31;
    unsigned char EffectDropDelay = 8;
    unsigned char i;
    unsigned char runintro = 0;

    HandyMusic_StopSoundEffect(236); // Chopper
    // Initialize scores from eeprom
    if (check_eeprom == 1) {
        runintro = 1;
        if (SDCheck == -1) {
            FRESULT res;
            res = LynxSD_OpenFileTimeout (SDSavePath);
            if (res == FR_OK) {
	            SDCheck = 1;
                LynxSD_CloseFile();
            } else {
                if (res == FR_DISK_ERR) {
	                SDCheck = 0;
	            } else {
	                SDCheck = -2;
                }
	        }
        }
        if (SDCheck == 1) {
            if (LynxSD_OpenFile(SDSavePath) == FR_OK) {
                LynxSD_ReadFile((void*)&completedmissions[0], 128);
                LynxSD_CloseFile();
            }
        } else {
            if (SDCheck == 0) {
                for (i = 0; i < 64; i++) {
                    unsigned int *buf = (unsigned int*)&completedmissions[0];
                    buf[i] = lnx_eeprom_read (i);
                }
            }
        }
	// Does the eeprom contain sensible data?
        if (completedmissions[0] > 50*60*60) {
            init_eeprom();
        }
        get_feelgood_and_rebel();
        check_eeprom = 0;
    }
    // Erase eeprom
    if (check_eeprom == 3) {
        init_eeprom();
        get_feelgood_and_rebel();
	check_eeprom = 2;
    }
    // Save scores to eeprom
    if (check_eeprom == 2) {
        set_feelgood_and_rebel();
        if (SDCheck == 1) {
            if (LynxSD_OpenFile(SDSavePath) == FR_OK) {
                LynxSD_WriteFile((void*)&completedmissions[0], 128);
                LynxSD_CloseFile();
            }
        }
        if (SDCheck == 0) {
            for (i = 0; i < 64; i++) {
                unsigned int *buf = (unsigned int*)&completedmissions[0];
                lnx_eeprom_write (i, buf[i]);
            }
        }
        check_eeprom = 0;
    }
    if (runintro == 0) {
        return;
    }
    tgi_setpalette(title_pal);
    blink = now + 60;
    blink2 = now + 20;
    HandyMusic_StopAll();
    if (bgmusic) {
        currentmusic = 0;
        HandyMusic_LoadPlayBGM(currentmusic);
    }
    while (endgame()) {
        while (tgi_busy())
            ;
        tgi_sprite(&Spr);
        drawSmoke();
        if (clock() > now + 5 * 60) {
            if (OnDrop) {
                if (OnDropDelay) {
                    OnDropDelay--;
                } else {
                    OnDrop--;
                    OnDropDelay = 8;
                }
            } else {
                OnDrop = 3;
            }
            drawOn(30, 20 - falling[OnDrop]);
        }
        if (clock() > now + 7 * 60) {
            if (DutyDrop) {
                if (DutyDropDelay) {
                    DutyDropDelay--;
                } else {
                    DutyDrop--;
                    DutyDropDelay = 8;
                }
            } else {
                DutyDrop = 3;
            }
            drawDuty(30 + 35, 20 - falling[DutyDrop]);
        }
        if (clock() > now + 2 * 60) {
            if (EffectDrop) {
                if (EffectDropDelay) {
                    EffectDropDelay--;
                } else {
                    EffectDrop--;
                    EffectDropDelay = 8;
                }
            } else {
                EffectDrop = 3;
            }
            drawEffect(30 - falling[EffectDrop], 30, 130 + falling[EffectDrop]);
        }
        if (clock() > blink) {
            drawLamps1();
            if (clock() > blink + 60)
                blink = clock() + 60;
        }
        if (clock() > blink2) {
            drawLamps2();
            if (clock() > blink2 + 20)
                blink2 = clock() + 20;
        }
        //handystatus();
        tgi_updatedisplay();
    }
    while (checkInput())
        ;
} 

