#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>

unsigned char *tes="huuhaa";
unsigned char dum = 0;

extern unsigned char planet[];
extern unsigned char titan[];
extern unsigned char spaceship[];

static SCB_REHV_PAL Sspaceship = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    spaceship,
    150, 60,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Stitan = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Sspaceship,
    titan,
    10, 4,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Splanet = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Stitan,
    planet,
    60, 60,
    0x80, 0x80,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

extern unsigned char checkInput();

static unsigned char bootpal[32] = {
    0x00,0x01,0x01,0x02,0x04,0x04,0x04,0x04,0x05,0x06,0x06,0x06,0x06,0x07,0x09,0x0A,
    0x00,0x22,0x32,0x04,0x36,0x09,0x28,0x18,0x19,0x29,0x39,0x68,0x0C,0x3C,0x4F,0x7E,
};

void boot(void) 
{
    clock_t now;
    clock_t shipnow;
    tgi_setpalette (bootpal);
    now = clock() + 2*60;
    shipnow = clock() + 1;
    while ((checkInput() == 0) && (Sspaceship.hpos > 80)) {
        while(tgi_busy())
            ;
        tgi_setcolor(COLOR_BLACK);
        tgi_bar(0, 0, 159, 101);
        tgi_sprite(&Splanet);
        tgi_updatedisplay();
        if (clock() > shipnow) {
            Sspaceship.hpos -= 1;
            shipnow = clock() + 10;
        }
        if (clock() > now) {
            Stitan.vpos -= 1;
            Splanet.hsize += 1;
            Splanet.vsize += 1;
            now = clock() + 8;
        }
    }
    while (checkInput())
        ;
} 

