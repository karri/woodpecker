import os
import numpy as np

print('.export _aah')
print('.segment "SAMPLES1_RODATA"')
print('_aah:')
fname='aah.raw'
with open(fname, 'rb') as f:
    data = f.read()
skipstart = 0
blank = 0
cnt = 0
for b in data:
    cnt = cnt + 1
    if (cnt > skipstart) and (cnt < 21000 + skipstart):
        if (cnt + skipstart < blank + skipstart):
            print('.byte ', 0)
        else:
            if b < 0:
                print('.byte ', 256 + b)
            else:
                print('.byte ', b)
