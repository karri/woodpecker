all:
	"$(MAKE)" -C resident;
	"$(MAKE)" -C keyhandler;
	"$(MAKE)" -C loader;
	"$(MAKE)" -C boot;
	"$(MAKE)" -C intro;
	"$(MAKE)" -C game;
	"$(MAKE)" -C music;
	"$(MAKE)" -C sfx;
	"$(MAKE)" -C samples;
	"$(MAKE)" -C cart clean;
	"$(MAKE)" -C cart;

clean:
	"$(MAKE)" -C resident clean;
	"$(MAKE)" -C keyhandler clean;
	"$(MAKE)" -C loader clean;
	"$(MAKE)" -C boot clean;
	"$(MAKE)" -C intro clean;
	"$(MAKE)" -C game clean;
	"$(MAKE)" -C music clean;
	"$(MAKE)" -C sfx clean;
	"$(MAKE)" -C samples clean;
	"$(MAKE)" -C cart clean;

